import { createStore, applyMiddleware } from "redux";
//import counterReducer from "./reducers/counterReducer";
import reducer from "./reducers";
import logger from './middlewares/logger'


export default createStore(
  //counterReducer,
  reducer,
  undefined,
  //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  applyMiddleware(logger)
);
