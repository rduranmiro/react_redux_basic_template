import { combineReducers } from "redux";
import counterReducer from "./counterReducer";

const reducer = combineReducers({
  value: counterReducer,
});

export default reducer;
