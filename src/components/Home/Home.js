import React from "react";
import { connect } from "react-redux";
import reactLogo from "../../assets/images/react-logo.png";
import reduxLogo from "../../assets/images/redux-logo.png";
import "./Home.css";

import {
  increment,
  decrement,
  reset,
} from "../../redux/actions/counterActions";
// import { INCREMENT, DECREMENT, RESET } from "../../redux/actions/types";

const Home = ({ counter, increment, decrement, reset }) => {
  return (
    <div className=" container Home">
      <div className='row'>
        <img
          className="Home__logo mb-3 mt-3"
          src={reactLogo}
          alt="react-logo"
        />
        <img
          className="Home__logo mb-3 mt-3"
          src={reduxLogo}
          alt="redux-logo"
        />
      </div>
      <button
        onClick={() => increment()}
        type="button"
        className="btn btn-primary mb-3 btn-block"
      >
        Increment
      </button>
      <button
        onClick={() => decrement()}
        type="button"
        className="btn btn-primary mb-3 btn-block"
      >
        Decrement
      </button>
      <h3>
        Counter <span className="badge badge-secondary mb-3">{counter}</span>
      </h3>
      <button
        onClick={() => reset()}
        type="button"
        className="btn btn-warning mb-3 btn-block"
      >
        Reset
      </button>
    </div>
  );
};

const mapStateToProps = (state) => ({
  //counter: state.counter,
  counter: state.value.counter, //using combineReducers must extract from asigned value
});

// Uncomment these lines to use mapDispatchtoProps

// const mapDispatchToProps = (dispatch) => ({
//   increment() {
//     dispatch({
//       type: INCREMENT,
//     });
//   },
//   decrement() {
//     dispatch({
//       type: DECREMENT,
//     });
//   },
//   reset() {
//     dispatch({
//       type: RESET,
//     });
//   },
// });

// Uncomment this line to use mapDispatchToProps
//export default connect(mapStateToProps, mapDispatchToProps)(Home);

// This line use imported actions from external file.
export default connect(mapStateToProps, { increment, decrement, reset })(Home);
